package KWIC.Data;

import java.io.IOException;

import KWIC.Pipe.Pipe;

public class DataSource  implements Runnable {
	
	private Pipe outPipe;
	private String allLines;
	
	public DataSource(String allLines, Pipe outPipe) {
		this.allLines = allLines;
		this.outPipe = outPipe;
	}

	public void run() {
		
		try {
			String[] lines = allLines.split("\n");
			
			for (int i = 0;i < lines.length;i++) {
				
				if (!lines[i].equals("")) 
					outPipe.write(lines[i] + "\n");
			}
			outPipe.write("\n"); 
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}

	}
}
