package KWIC.Data;

import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JTextArea;

import KWIC.Listener.SinkListener;
import KWIC.Pipe.Pipe;
import KWIC.ui.GUI;

public class DataSink implements Runnable  {

	private Pipe inPipe;
	private String outputStr;
	private  boolean notFinished = true;
	private ArrayList<SinkListener> listener;
	
	public DataSink(Pipe inPipe) {
		this.inPipe = inPipe;
		outputStr = "";
		listener = new ArrayList<SinkListener>();
	}

	public void addListener(GUI gui){
		this.listener.add(gui);
	}
	
	public void notifyListener(){
		for(SinkListener sl : listener){
			sl.sinkFinished(outputStr);
		}
	}

	public void run() {
		String temp;
		try {
			while (notFinished) {
				temp = readLine();
				if(!temp.equals("\n")){
					outputStr += temp;
				}else{
					notifyListener();
					notFinished = false;
				}
			}
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	private String readLine() throws IOException {
		String line = "";
		int c = inPipe.read();
		while(true) {
			char ch = (char) c;
			line += ch;
			if (ch == '\n') break;
			c = inPipe.read();
		}
		return line;
	}
	
	public String getOutput(){
		return outputStr;
	}
}
