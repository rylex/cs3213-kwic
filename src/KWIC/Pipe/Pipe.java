package KWIC.Pipe;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.Reader;

public class Pipe {

	private PipedReader reader;
	private PipedWriter writer;
	
	public Pipe() throws IOException {
		reader = new PipedReader();
		writer = new PipedWriter();
		writer.connect(reader);
	}
	
	public void write(String data) throws IOException{
		writer.write(data);
		writer.flush();
	}
	
	public int read() throws IOException{
		return reader.read();
	}
	
}
