package KWIC.ui;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import java.awt.GridLayout;
import java.awt.FlowLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

import KWIC.Data.DataSink;
import KWIC.Data.DataSource;
import KWIC.Filter.AlphabetizerFilter;
import KWIC.Filter.CaseFilter;
import KWIC.Filter.CircularShiftFilter;
import KWIC.Filter.IgnoreFilter;
import KWIC.Listener.SinkListener;
import KWIC.Pipe.Pipe;

public class GUI implements SinkListener{

	private JFrame frame;
	private JTextArea txtAreaInput;
	private JTextArea txtAreaOutput;
	private JTextArea txtAreaIgnore;
	private DataSink ds;
	private static GUI window;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		txtAreaInput = new JTextArea();
		txtAreaInput.setLineWrap(true);
		JScrollPane scrollInput = new JScrollPane(txtAreaInput); 
		scrollInput.setBounds(10, 37, 435, 178);
		frame.getContentPane().add(scrollInput);

		JLabel lblInput = new JLabel("Input Lines (separate lines with newline)");
		lblInput.setBounds(10, 22, 270, 14);
		frame.getContentPane().add(lblInput);

		JLabel lblWordsToIgnore = new JLabel("Words to Ignore");
		lblWordsToIgnore.setBounds(455, 20, 119, 14);
		frame.getContentPane().add(lblWordsToIgnore);

		JLabel lblOutput = new JLabel("Output");
		lblOutput.setBounds(10, 271, 46, 14);
		frame.getContentPane().add(lblOutput);

		JButton btnCirculateKeywords = new JButton("Circulate Keywords");
	
		
		btnCirculateKeywords.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				startProcess();
			}
		});
		
		
		btnCirculateKeywords.setBounds(217, 227, 163, 37);
		frame.getContentPane().add(btnCirculateKeywords);

		txtAreaOutput = new JTextArea();
		txtAreaOutput.setLineWrap(true);
		JScrollPane scrollOutput = new JScrollPane(txtAreaOutput); 
		scrollOutput.setBounds(10, 286, 574, 274);
		
		frame.getContentPane().add(scrollOutput);

		txtAreaIgnore = new JTextArea();
		JScrollPane scrollIgnore = new JScrollPane(txtAreaIgnore); 
		scrollIgnore.setBounds(455, 37, 129, 178);
		frame.getContentPane().add(scrollIgnore);
	}
	
	private void startProcess(){
		txtAreaOutput.setText("");
		try {
			Pipe pipe1 = new Pipe();
			Pipe pipe2 = new Pipe();
			Pipe pipe3 = new Pipe();
			Pipe pipe4 = new Pipe();
			Pipe pipe5 = new Pipe();

			Thread t1 = new Thread(new DataSource(txtAreaInput.getText().trim(), pipe1));
			Thread t2 = new Thread(new CircularShiftFilter(pipe1, pipe2));
			Thread t3 = new Thread(new IgnoreFilter(pipe2, pipe3, txtAreaIgnore.getText().trim()));
			Thread t4 = new Thread(new CaseFilter(pipe3, pipe4));
			Thread t5 = new Thread(new AlphabetizerFilter(pipe4, pipe5));
			Thread t6 = new Thread(ds = new DataSink(pipe5));
	
			ds.addListener(this);			//Listener for when output is ready to be collected
			
			t1.start();
			t2.start();
			t3.start();
			t4.start();
			t5.start();
			t6.start();
		} catch (IOException ex) {
			System.out.println("PipelineError: " + ex.getMessage());
		} 	
	
	}
	
	@Override
	public void sinkFinished(String str) {
		txtAreaOutput.append(str);
	}

}
