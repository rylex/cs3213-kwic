package KWIC.Filter;
import java.io.IOException;

import KWIC.Pipe.Pipe;

public abstract class Filter implements Runnable{

	private Pipe input;
	private Pipe output;
	protected boolean notFinished = true;
	
	public Filter(Pipe in, Pipe out) {
		this.input = in;
		this.output = out;
	}
	
	abstract protected void Process() throws IOException;

	public void run(){
		try{
			while(notFinished)
				Process();
		} catch (IOException ex){
			System.out.println("filter error:" + ex.getMessage());
		}
	}
	
	protected String readLine() throws IOException {
		String line = "";
		int c = input.read();
		while(true) {
			char ch = (char) c;
			line += ch;
			if (ch == '\n') break;
			c = input.read();
		}
		return line;
	}
	
	protected char read() throws IOException {
		return (char) input.read();
	}
	
	protected void write(String str) throws IOException {
		output.write(str);
	}

}
