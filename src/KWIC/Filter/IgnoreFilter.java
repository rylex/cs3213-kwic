package KWIC.Filter;

import java.io.IOException;
import KWIC.Pipe.Pipe;

public class IgnoreFilter extends Filter  {

	private static final char[] symbols = {',', '.', ':', '-', '?','=','&','!','#','$','~','`'};
	private String[] ignoreList;

	public IgnoreFilter(Pipe inPipe, Pipe outPipe, String ignoreList) {
		super(inPipe,outPipe);
		this.ignoreList = ignoreList.split("\n");
	}

	protected void Process() throws IOException {
		String line = readLine();
		
		if (!line.equals("\n")) {
			int pos = line.indexOf(" ");
			if (pos >= 0) {
				String firstWord = line.substring(0, pos);
				firstWord = removeSymbol(firstWord).trim();
				if (!isIgnore(firstWord) && firstWord.length() > 0) {
					line = firstWord + line.substring(pos);
					write(line);
				} 
			}
		}else {
			write("\n");
			notFinished = false;
		}
	}

	private String removeSymbol(String word) {
		char lastChar = word.charAt(word.length() - 1);
		for (char c : symbols) {
			if (c == lastChar) return word.substring(0, word.length() - 1);
		}
		return word;
	}

	private boolean isIgnore(String word) {
		for (String ignore : ignoreList) {
			if (ignore.equalsIgnoreCase(word)) {
				return true;
			}
		}
		return false;
	}

}
