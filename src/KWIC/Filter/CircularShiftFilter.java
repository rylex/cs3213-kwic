package KWIC.Filter;

import java.io.*;

import KWIC.Pipe.Pipe;

public class CircularShiftFilter extends Filter {

	public CircularShiftFilter(Pipe in, Pipe out){ 
		super(in,out); 
	}

	protected void Process() throws IOException {
		String lineOfWord = readLine();
		if(!lineOfWord.equals("\n")){
			String[] store = lineOfWord.split("\\s"); 	// create array to store words, split by white space

			for (int i = 0; i < store.length; i++) {
				if (store[i].equals("")) 							//check for empty string;
					continue; 				

				String string = store[i] + " "; 				// getting first word token

																					// words after first word
				for (int j = i + 1; j < store.length; j++) {
					string += store[j] + " "; 						// append more words behind
				}
				
				int before = 0;
				for (int j = 0; j < i; j++) {
					string += store[j] + " ";
					before++;
				}
		
				write(string + "\n");
			}
		}else{
			write("\n");
			notFinished = false;
		}
	}
}
