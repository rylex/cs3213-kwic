package KWIC.Filter;

import java.io.IOException;

import KWIC.Pipe.Pipe;

public class CaseFilter extends Filter {

	public CaseFilter(Pipe in, Pipe out){ 
		super(in,out); 
	}

	protected void Process() throws IOException {

		String line = readLine();	
		if(line.equals("\n")){
			write("\n");
			notFinished = false;
		}else{
			String s = line.substring(0, 1).toUpperCase() + line.substring(1).toLowerCase();
			write(s);
		}
	}
}
