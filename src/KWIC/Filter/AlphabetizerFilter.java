package KWIC.Filter;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.List;
import java.util.ArrayList;

import KWIC.Pipe.Pipe;

public class AlphabetizerFilter extends Filter {

	TreeSet<String> lines = new TreeSet<String>();
	//	List<String> lines = new ArrayList<String>();
	public AlphabetizerFilter(Pipe in, Pipe out)  {
		super(in,out); 
	}

	@Override
	protected void Process() throws IOException {
		String line = readLine();

		if(line.equals("\n")){
			//	Collections.sort(lines, new alphabetComparator());
			Iterator<String> it = lines.iterator();
			while(it.hasNext()){
				write(it.next());
			}
			for (String out : lines) {
				write(out);
			}
			write("\n");
			notFinished = false;
		}else
			lines.add(line);
	}

}

/*class alphabetComparator implements Comparator<String>{				//Required for arraylist sorting

	@Override
	public int compare(String o1, String o2) {
		return o1.compareToIgnoreCase(o2);
	}


}*/
