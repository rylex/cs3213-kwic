The Day after Tomorrow
Fast and Furious
Man of Steel
Starman
Terror Of Mechagodzilla
That's What I Am
The Big Chill
The Big Hit
The Buddy Holly Story
The City Of Lost Children
The Human Stain
Ichi The Killer
Kill Bill Volume 1
Kill Bill Volume 2
Knucklehead
La Bamba
The Jewel Of The Nile
The Juror
The Legend Of Drunken Master
all i want
bed of roses
candyman
cursed
difo
forest gump
Goldfinger (007 James Bond)
12 Years a Slave
127 Hours
13 Going on 30
13th Warrior, The
1408
17 Again
20,000 Leagues Under the Sea
2001: A Space Odyssey
2012
2046
21 Grams
25th Hour
27 Dresses
28 Days Later
28 Weeks Later
3 Idiots
300
3000 Miles to Graceland
3:10 to Yuma
40 Year-Old Virgin, The
400 Blows, The
50/50
50 First Dates
(500) Days of Summer
Alvin and the Chipmunks: The Squeakquel
Alvin and the Chipmunks: Chipwrecked
All About Steve
All Dogs Go to Heaven
All of Me
All the President's Men
All the Right Moves
Almost Famous
Along Came a Spider
Alpha Dog
Alpha and Omega
The Amazing Spider-Man 
The Amityville Horror 
American Beauty
American Chai
American Gangster
American Graffiti
American History X
American in Paris, An
American Me
American Pie
American Pie 2
American Pie Presents: The Book Of love
American Psycho
American Tail, An
American Tail: Fievel Goes West, An
American Wedding
Americanization of Emily, The
Bee Movie
Bee Season
Beerfest
Beetlejuice
Before Midnight
Before Sunrise
Before Sunset
Beginners
Behind Enemy Lines
Being John Malkovich
Being There
Be Kind Rewind
Believer, The
Ben-Hur
Bend It Like Beckham
Benny & Joon
Beowulf
Best Little Whorehouse in Texas, The
Best in Show
Best Years of Our Lives, The
Better Off Dead
Better Than Chocolate
Better Tomorrow, A
Beverly Hills Chihuahua
Beverly Hills Cop II
Beverly Hills Cop III
Beyond the Valley of the Dolls
Bicentennial Man
Bicycle Thieves
Big
Big Chill, The
Big Daddy
Big Easy, The
Big Fish
Big Lebowski, The
Big Night
Big Sleep, The
Big Top Pee-wee
Big Trouble
Big Trouble in Little China
Bill and Ted's Excellent Adventure
Billy Madison
Birds, The
Black Book
Black Cat, The
Black Christmas
Black Christmas
Black Hawk Down
Black Hole, The
Black Rain
Black Sheep
Black Swan
Blade
Blade II
Blade: Trinity
Blade Runner
Blades of Glory
Blair Witch Project, The
Confessions of a Dangerous Mind
Confetti
Conquest of Space
Conspiracy Theory
Constant Gardener, The
Cool and the Crazy‎
Cool Hand Luke
Cool Runnings
Cool World
Coraline
Core, The
Corpse Bride
Contact
Court Jester, The
Count of Monte Cristo, The
Cowboy Bebop: The Movie
Coyote Ugly
Craft, The
Crank
Dracula AD 1972
Dracula: Dead and Loving It
Dragnet
Dreamers, The
Driving Miss Daisy
Drop Dead Fred
Drop Dead Gorgeous
Drowning Mona
Drugstore Cowboy
Duck Soup
Dude, Where's My Car?
Duel
Dukes of Hazzard, The
Dumb and Dumber
Dumb and Dumberer
Dumbo
Dummy
Dune
Duplex
Dylan Dog: Dead of Night
Deep Blue Sea
Femme Fatale
FernGully: The Last Rainforest
Ferris Bueller's Day Off
Few Good Men, A
Fiddler on the Roof
Field of Dreams
Fifth Element, The
Fight Club
Fighter, The
Final Destination
Final Destination 2
Final Destination 3
The Final Destination
Final Destination 5
Final Fantasy: The Spirits Within
Final Fantasy VII: Advent Children
Finding Forrester
Finding Nemo
Finding Neverland
Fine Mess, A
Firm, The
First Blood
First Knight
First Wives Club, The
Fish Called Wanda, A
Fisher King, The
Fistful of Dollars, A
Five Easy Pieces
Five Minutes of Heaven
Flags of Our Fathers
Flash Gordon
Flatliners
Fletch
Fletch Lives aka Fletch 2
Flicka
Gravity
Great Dictator, The
Great Expectations
Grease
Grease 2
Great Debaters, The
Great Escape, The
The Great Mouse Detective
The Great Outdoors
The Greatest Game Ever Played
Green for Danger
Green Lantern
Green Mile, The
Green Zone
Greenberg
Gremlins
Grey Zone, The
Grifters, The
Grindhouse
Groundhog Day 
Grosse Pointe Blank
Grown Ups 
Grumpy Old Men
Guarding Tess
Guess Who
Guess Who's Coming to Dinner
Gun Crazy
Gung Ho

Home for the Holidays
Homeward Bound: The Incredible Journey
Homeward Bound II: Lost in San Francisco
Honey, I Shrunk The Kids
Honeymoon in Vegas
Hoodwinked
Hook
Hoosiers
Horror Express
Horse Feathers
Hospital, The
Hostel
Hostel: Part II
Hotel Rwanda
Hot Chick, The
Hot Fuzz
Hot Rod
Hot Shots! Part Deux
Hot Shots
Hours, The
The House Bunny
In & Out
In a Lonely Place
In Bruges
In Cold Blood
In the Heat of the Night
In the Line of Fire
In The Loop
Incredible Hulk, The
In Her Shoes
In the Mood for Love
In the Name of the Father
Into the Blue
Into the Wild
Inception
Incredibles, The
Inconvenient Truth, An
Independence Day
Inferno
Indiana Jones and the Kingdom of the Crystal Skull
Indiana Jones and the Last Crusade
Indiana Jones and the Temple of Doom
Inglorious Basterds
Innerspace
Inside Man
Inside Monkey Zetterland
Inspector Gadget
Inspector Gadget 2
Interview with the Vampire
Intolerable Cruelty